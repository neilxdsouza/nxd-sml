fun double(x:int):int = x * 2;
fun square(x:int):int = x * x;
fun power(x:int, y:int):int = if (y=0) then 1 else x * power(x,y-1);
