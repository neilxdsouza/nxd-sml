abstype abs_color = Red | Blue | Yellow
with 
        val elmoColor = Red
        val groverColor = Blue
end
