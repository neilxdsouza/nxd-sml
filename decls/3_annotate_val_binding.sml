let 
  val a = 3 + 4 : int
in
  (a * a) - a
end
