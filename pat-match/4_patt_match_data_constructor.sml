datatype int_or_real = I of int | R of real;
val a = I 3;
val (I i) = a;
