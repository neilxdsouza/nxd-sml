let
  val x = 10
in
  case x
    of 0 => "zero"
     | 1 => "one"
     | _ => "out of range"
end

